import gzip
import importlib
import json
import random
import re
from pathlib import Path

import koji
import pytest
import requests_mock
from hamcrest import assert_that, equal_to

# Since our script has a dash, we have to use importlib to import it
makelockfile = importlib.import_module("make-lockfile")
TEST_DIR = Path(__file__).parent

HEX_DIGIT_REGEX = "[0-9a-fA-F]"

PRIMARY_XML_STEM_REGEX = (
    f"(?P<path>[\\w/]*)/{HEX_DIGIT_REGEX}{{64}}-primary\\.xml\\.gz"
)
REPOMD_XML_STEM_REGEX = "(?P<path>[\\w/]*)/repomd\\.xml"

RHIVOS_TAG = "rhivos-1.0.0-gate"


def repomd_xml(distro):
    test_repomd = TEST_DIR / "data" / f"test-{distro}-x86_64-baseos-repomd.xml"
    return test_repomd.read_bytes()


def primary_xml(distro):
    test_primary = (
        TEST_DIR / "data" / f"test-{distro}-x86_64-baseos-primary.xml"
    )
    return gzip.compress(test_primary.read_bytes())


def content_tree_json():
    test_content_file = TEST_DIR / "data" / "test-content-tree.json"
    return test_content_file.read_bytes()


@pytest.fixture
def random_repo(request):
    distro = request.param
    choices = makelockfile.DEFAULT_REPOS[distro]
    choice = random.choice(choices)
    return choice, distro


# Tests
@pytest.mark.parametrize("distro", ["cs9", "rhel9"])
def test_get_package_names_from_json(distro):
    test_content_file = TEST_DIR / "data" / "test-content-tree.json"
    with test_content_file.open("r") as json_file:
        test_content_tree = json.load(json_file)

    expected_names_file = (
        TEST_DIR / "data" / f"expected-{distro}-package-names-x86_64.json"
    )
    with expected_names_file.open("r") as json_file:
        expected_names = json.load(json_file)

    actual_package_names = makelockfile.get_package_names_from_json(
        test_content_tree, distro
    )
    assert actual_package_names == set(expected_names)


# There are only repos defined for cs9
@pytest.mark.parametrize("random_repo", ["cs9"], indirect=True)
def test_get_nvrs(random_repo):
    repo, distro = random_repo
    with requests_mock.Mocker() as mock:
        for xml_file_type in ("repomd", "primary"):
            if xml_file_type == "repomd":
                get_xml_method = repomd_xml
                stem_regex = REPOMD_XML_STEM_REGEX
            else:
                get_xml_method = primary_xml
                stem_regex = PRIMARY_XML_STEM_REGEX

            uri_regex = re.compile(f"{re.escape(repo)}/{stem_regex}")

            mock.register_uri(
                "GET",
                uri_regex,
                status_code=200,
                content=get_xml_method(distro),
            )

        expected_nvrs_file = TEST_DIR / "data" / f"expected-{distro}-nvrs.json"
        with expected_nvrs_file.open("r") as json_file:
            expected_nvrs = json.load(json_file)

        # use only a single, randomly chosen repo to shorten test duration
        # the test is setup such that all repos have their XML responses mocked
        # to what is in our test files, so iterating over all of them really just
        # tests the same thing len(repos) times
        actual_nvrs = makelockfile.get_nvrs([repo])
        assert_that(actual_nvrs, equal_to(expected_nvrs))


def test_get_nvrs_with_multirepos():
    # Test a usecase where kernel-automotive is in two repos and we want to ensure get_nvrs
    # returns the newest nvr from the conjoined set
    distros = {
        "autosd": "https://buildlogs.centos.org/9-stream/autosd/x86_64/packages-main",
        "automotive": "https://buildlogs.centos.org/9-stream/automotive/x86_64/packages-main",
    }
    with requests_mock.Mocker() as mock:
        for distro, repo in distros.items():
            for xml_file_type in ("repomd", "primary"):
                if xml_file_type == "repomd":
                    get_xml_method = repomd_xml
                    stem_regex = REPOMD_XML_STEM_REGEX
                else:
                    get_xml_method = primary_xml
                    stem_regex = PRIMARY_XML_STEM_REGEX

                uri_regex = re.compile(f"{re.escape(repo)}/{stem_regex}")

                mock.register_uri(
                    "GET",
                    uri_regex,
                    status_code=200,
                    content=get_xml_method(distro),
                )

        nvrs = makelockfile.get_nvrs(distros.values())
        assert "kernel-automotive" in nvrs
        assert nvrs["kernel-automotive"] == [
            "0",
            "5.14.0",
            "107.68.el9s",
            "kernel-automotive-5.14.0-107.68.el9s.src",
        ]


def test_get_envfile_http():
    with requests_mock.Mocker() as mock:
        mock.register_uri(
            "GET",
            "http://test.json",
            status_code=200,
            content=content_tree_json(),
        )
        env_json = makelockfile.get_envfile("http://test.json")

    test_content_file = TEST_DIR / "data" / "test-content-tree.json"
    with test_content_file.open("r") as json_file:
        test_content_tree = json.load(json_file)

    assert env_json == test_content_tree


def test_get_envfile_local():
    test_content_file = TEST_DIR / "data" / "test-content-tree.json"
    env_json = makelockfile.get_envfile(str(test_content_file))

    with test_content_file.open("r") as json_file:
        test_content_tree = json.load(json_file)
    assert env_json == test_content_tree


def test_merge_packages_names_and_nvrs():
    vr_a = ["0", "10", "8-1"]
    vr_b = ["0", "100", "0"]
    vr_c = ["0", "a", "a"]

    nvrs = {"a": vr_a, "b": vr_b, "c": vr_c}

    # No Koji
    test_set = makelockfile.merge_packages_names_and_nvrs({"a", "b"}, nvrs)
    assert {f"a-{vr_a[1]}-{vr_a[2]}", f"b-{vr_b[1]}-{vr_b[2]}"} == test_set

    # No Koji
    test_set = makelockfile.merge_packages_names_and_nvrs({"c"}, nvrs)
    assert {f"c-{vr_c[1]}-{vr_c[2]}"} == test_set


@pytest.mark.parametrize("distro", ["cs9", "rhel9"])
def test_create_lockfile(distro):
    nvr_a = "a-10.8-1"
    nvr_b = "b-100-0"
    nvr_c = "c-a-a"
    nvr_d = "d-b-b"
    srpm_a = "a-10.8-1.src"
    srpm_b = "b-100-0.src"
    srpm_c = "c-a-a.src"
    srpm_d = "d-b-b.src"

    packages_aarch64 = {nvr_a, nvr_b, nvr_c}
    packages_x86 = {nvr_a, nvr_b, nvr_d}
    list_of_srpm = {srpm_a, srpm_b, srpm_c, srpm_d}

    output = makelockfile.create_lockfile(
        distro, packages_aarch64, packages_x86, list_of_srpm
    )
    expected_output = {
        distro: {
            "common": [
                nvr_a,
                nvr_b,
            ],
            "arch": {
                "aarch64": [
                    nvr_c,
                ],
                "x86_64": [
                    nvr_d,
                ],
            },
            "src": [
                srpm_a,
                srpm_b,
                srpm_c,
                srpm_d,
            ],
        }
    }
    assert expected_output == output


def test_get_nvrs_from_koji():
    expected_out = {
        "kernel-automotive": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-kvm": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debug-debuginfo": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debuginfo": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-devel-matched": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debug": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-selftests-internal": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debug-modules-extra": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-devel": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debug-modules-internal": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debug-modules": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debug-devel": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-modules-extra": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-modules": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debuginfo-common-x86_64": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-modules-internal": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debug-kvm": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debug-devel-matched": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debug-core": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-core": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
        "kernel-automotive-debuginfo-common-aarch64": [
            None,
            "5.14.0",
            "128.90.el9iv",
            "kernel-automotive-5.14.0-128.90.el9iv.src",
        ],
    }

    koji.ClientSession = my_mock_session
    result = makelockfile.get_nvrs_from_koji("mock", RHIVOS_TAG)

    assert result == expected_out


def test_get_srpm_from_koji():
    koji.ClientSession = my_mock_session
    result = makelockfile.get_srpm_from_koji(
        koji.ClientSession, "kernel-automotive-core-5.14.0-128.90.el9iv.arch64"
    )

    assert result == "kernel-automotive-5.14.0-128.90.el9iv.src"


def my_mock_session(url, opts):
    if url != "":
        new_session = koji.ClientSession
        new_session.listTagged = my_list_tagged
        new_session.listPackages = my_list_packages
        new_session.getTag = my_get_tag
        new_session.listBuilds = my_list_builds
        new_session.listRPMs = my_list_rpms
        new_session.getRPM = my_get_rpm
        new_session.listBuildRPMs = my_list_build_rpms
        return new_session
    return None


def my_list_build_rpms(rpm_info):
    if rpm_info:
        return [
            {
                "arch": "aarch64",
                "build_id": 41729,
                "buildroot_id": 160253,
                "buildtime": 1667326951,
                "epoch": None,
                "external_repo_id": 0,
                "external_repo_name": "INTERNAL",
                "extra": None,
                "id": 404651,
            },
            {
                "arch": "src",
                "build_id": 41729,
                "buildroot_id": 160252,
                "buildtime": 1667326804,
                "epoch": None,
                "external_repo_id": 0,
                "external_repo_name": "INTERNAL",
                "extra": None,
                "id": 404624,
                "metadata_only": False,
                "name": "kernel-automotive",
                "nvr": "kernel-automotive-5.14.0-128.90.el9iv",
                "payloadhash": "52d14cbc2daa71468fd39c85e5fb93",
                "release": "128.90.el9iv",
            },
        ]


def my_get_rpm(nvra):
    if nvra:
        return {
            "arch": "aarch64",
            "build_id": 41729,
            "buildroot_id": 160253,
            "buildtime": 1667326951,
            "epoch": None,
            "external_repo_id": 0,
            "external_repo_name": "INTERNAL",
            "extra": None,
            "id": 404660,
            "metadata_only": False,
            "name": "kernel-automotive-modules",
            "payloadhash": "4d7dc21ffe4e636917ae...696da5f10a",
            "release": "128.90.el9iv",
            "size": 22893765,
        }


# Not exercised but left in case it is in the future
def my_get_tag(tag):
    if tag == RHIVOS_TAG:
        return {
            "arches": "",
            "id": 104788,
            "locked": False,
            "maven_include_all": False,
            "maven_support": False,
            "name": "rhivos-1.0.0",
            "perm": None,
            "perm_id": None,
            "extra": {},
        }
    return {}


# Not exercised but left in case it is in the future
def my_list_packages(tag):
    packages = [
        {
            "blocked": False,
            "extra_arches": "",
            "owner_id": 6182,
            "owner_name": "scweaver",
            "package_id": 82826,
            "package_name": "kernel-automotive",
            "tag_id": 104788,
            "tag_name": "rhivos-1.0.0",
        }
    ]
    for pkg in packages:
        if tag == pkg["tag_name"]:
            return [pkg]
    return []


def my_list_tagged(
    tag,
    event=None,
    inherit=False,
    prefix=None,
    latest=False,
    package=None,
    owner=None,
    type=None,
    strict=True,
    extra=False,
):
    f = open(f"{TEST_DIR}/data/koji-list-tagged-builds-rhel-9.json")
    builds = json.load(f)

    if latest:
        latest_builds = []
        packages = set([x["package_name"] for x in builds])
        for package in packages:
            package_builds = list(
                filter(lambda build: build["package_name"] == package, builds)
            )
            latest_builds.append(
                max(package_builds, key=lambda x: x["build_id"])
            )
        builds = latest_builds

    return builds


# Not exercised but left in case it is in the future
def my_list_builds(package_id):
    f = open(f"{TEST_DIR}/data/koji-builds-rhel-9.json")
    builds = json.load(f)

    for build in builds:
        if package_id == build["package_id"]:
            return builds

    return []


def my_list_rpms(build_id):
    f = open(f"{TEST_DIR}/data/koji-rpms-rhel-9.json")
    rpms = json.load(f)

    for rpm in rpms:
        if rpm["build_id"] == build_id:
            return rpms
    return []
